# Buddhism site
Hosted at [https://buddhism-wiki.netlify.com](https://buddhism-wiki.netlify.com).

# Tutorial
1. install [zim wiki](https://zim-wiki.org/)
2. try to export it using template in template folder
3. open 主页.html using your browser

# To contribute
You can create a merge request.

What you can contribute:
* improve html template
* fix typo in the texts
* improve texts (e.g. add table of contents if any, 科判)
* add more 经典 and more notes by creating sub pages

# contact me
[contactme@alexlai.xyz](mailto:contactme@alexlai.xyz)
